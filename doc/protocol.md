# Hack Experience Lab Protocol

The goal of the Hack Experience Lab protocol is to provide the ability for multiple terminals to connect to the same Hack Experience, to synchronize the state of the current Scenario between all terminals, and basically allow for an interactive and reactive experience.

Considering the variety of terminals the Hack Experience Lab intends to support, the protocol needs to be able to implemented on multiple platforms without the need to implement low-level networking over and over again.

The networking for the Hack Experience Lab will be done using the websocket protocol ([RFC6455](https://www.rfc-editor.org/rfc/rfc6455)). The websocket protocol provides a message-based transport layer that is well-supported on all main platforms and by open source libraries, allowing the implementation to focus on the messages instead of the networking aspect.

## Glossary

| Term              | Description |
|-------------------|-------------|
| Challenge         | A puzzle or task inside a "Scenario". Every scenario usually contains multiple challenges.
| Client            | Any application that communicates with "Server".
| Device            | Any hardware component hosting a "Terminal".
| Hacker            | Participant in an Experience. Usually works in teams. Expected to be humanoid.
| HEL               | Hack Experience Lab. Title for any physical location hosting Hack Experiences.
| Scenario          | A storyboard defining the environment and goal of a single Hack Experience.
| Server            | HTTPS-based centrally-hosted component. Maintains "Session" state and synchronizes between all connected "Terminal".
| Session           | The gamestate of a currently running "Scenario".
| Static Content    | Resource files for Scenarios and Terminal Functions
| Terminal          | Any application that maintains an active connection with the websocket on "Server". "Terminal" is a subset of "Client".
| Terminal Function | A single interactive element of a "Scenario", hosted by "Terminal". Usually a 1-to-1 relationship but not required.

# Concepts

## Hardcoded scenario behaviours

The goal of the Hack Experience Lab is to provide a tailored and impactful experience to our Hackers, and not to be an extremely configurable/flexible Experience hosting machine. By limiting the configurability of the Lab, the system is allowed to hardcode certain behaviours and expectations directly into the Terminal Functions. This gives the following advantages:

1. Scenario state can be advanced by communicating through major events, instead of needing to manipulate state in tiny details.
   * For example, an event can state "Scenario is now in Stage 5". Every Terminal Function can then decide for itself what it needs to do in stage 5, instead of requiring an external script to manipulate the individual state of every Terminal Function to put them in the correct mode.
2. Logic can be implemented in the best way possible for every Terminal Function, instead of having to deal with some form of common scripting language that can limit the full power of whatever frontend the Terminal is using.
   * For example, a 3D engine can implement behaviour natively while a HTML5 terminal can use typescript or nodejs or REST calls or whatever is needed.

Ofcourse, certain configurability is still advisable to allow Terminal Function reusability between Scenario's, but this should not limit the experience of our Hackers.

## Interchangeability

By putting all logic inside the Terminal Functions, all other software components can be kept dumb and freely interchangeable.

* Server software is not specific to a Scenario. It needs to host static content and maintain state. Since the state is only relevant to the Terminal Functions, the state can be considered a black box. Server can thus host any Scenario without requiring Scenario-specific knowledge.
* Terminal software is not specific to a Scenario. It needs to have enough knowledge to connect to Server and bind itself to a specific Terminal Function based on the capabilities of the Device it is running on (e.g. "Mobile", "Fixed", "Touchscreen", "Pointer", "2D", "3D", "XR"). After that, all Scenario-specific knowledge will be handled by the attached Terminal Function.
* Device may or may not need a platform-specific installation of Terminal-software. Since the Terminal software is not specific to a Scenario, by extension neither is a Device.

This design choice gives us the following advantages:

1. When a Device breaks, it can be replaced by any Device with similar capabilities with no reconfiguration needed.
2. Allows for BYOD scenarios.
3. Any update to a Scenario only requires updating the Static Content and/or creating new Terminal Functions. Server and Terminals do not have to be updated.

## Equality

Although every Client may fulfill a different role within a Scenario, all Clients are deemed equal: all protocol messages and all Scenario functions/events are available to all valid Clients. This ensures a Scenario is not artificially limited in delivering its experience.

## Separation between HEL and Hack Challenges

Most Scenario's will require the Hackers to perform a security exploit of one kind or the other. These challenges can go as far as attaining root-privileges on certain hosted machines. The HEL needs to be separated from the Hacker Challenges so that the Experience itself will not be compromised by some of the overzealous participants.

This means that any Hacking challenges MUST NOT be hosted on Server. Actual air-gapping between Experience and Challenge is reponsibility of the Terminal Functions.

# Architecture

The Hack Experience Lab protocol is based on a server-client architecture. Every client is an application connected to the server.

```plantuml
@startuml

component "Device 1" {
 node "Terminal 1" {
  node "Terminal Function 1"
 }
}
component "Device 2" {
 node "Terminal 2" {
  node "Terminal Function 2"
 }
}
component "PC Browser" {
  node "Client" {
    node "Admin interface"
  }
}
component "Server" {
  node "Websocket"
  node "Static content"
}

"Admin interface" -> "Server"
"Websocket" <-=> "Terminal 1"
"Websocket" <-=> "Terminal 2"
"Static content" <- "Terminal Function 1"
"Static content" <- "Terminal Function 2"

@enduml
```

All interactive communication is handled over Websocket connection using the defined Protocol. This document does not describe any other (e.g. REST-like) interfaces that may or may not be desired by specific Scenarios and Challenges.

## Protocol Actions

Every Client can perform the following actions:
- Connect to Server
- Login to the Administrative interface and promote itself to Admin
- Login to a specific Session and promote itself to Terminal

Any Administrator (= Client promoted to Admin) can:
- List all active Sessions
- Create a new Session based on a Scenario
- Reset an active Session
- Stop an active Session

Any Terminal (= Client promoted to Terminal) can:
- List all Terminal Function of the current Session
- Attach to a specific Terminal Function
- Receive attach and detach events from other Terminals

Any Terminal Function (= Terminal attached to a Terminal Function) can:
- Send Scenario events
- Receive Scenario events from other Terminal Functions
- Update the Session state
- Request full Session state

## Protocol Messages

The protocol defines a set of messages in order to implement all mentioned Protocol Actions. These messages have the following characteristics:

1. Exactly one Protocol Message per Websocket message.
   * Since the transport layer (Websocket) is message-based, restricting the Protocol to use one message per Websocket message gives us perfectly delimited messages.
2. Messages can not be split over multiple Websocket messages.
   * Framing is handled transparently by the Websocket transport layer.
3. Message are a single word header followed by a single space followed by a single valid JSON structure.
   * The header defines the type of message, allowing Terminals to select the proper parser for the JSON structure that follows.

In the description below, C -> S represents a message from the client C (device) is sent to server S. When applicable, C is extended with a number to indicate a specific client, e.g., C1, C2, etc. The keyword "others" is used to indicate all other clients except for the client who made the request.

# Protocol Message Definitions

Note: message examples may have added whitespace inside the JSON structure for legibility but the whitespace is not required by the protocol.

Text between "<" and ">" are placeholders. The following placeholders are used:

| Placeholder            | Description | Example |
|------------------------|-------------|---------|
| `<error code>`         | (int) Failure code of the last Client Action | 5000
| `<error header>`       | (string) Message header name of the message that caused the error | "LOGIN", "SESSION_CREATE"
| `<error message>`      | (string) Textual message belonging to the provided error code  | "Wrong password"
| `<password>`           | (string) A password | ******
| `<role>`               | (string) Role that Client intends to perform | "admin", "terminal"
| `<scenario name>`      | (string) The name of the scenario | "Substation Sabotage"
| `<session name>`       | (string) Free-form description of the session | "Alice and Bob attempt 69"
| `<server version>`     | (string) The semantic version number of the server software | "3.1.41"
| `<terminal function>`  | (string) Terminal function type | "numpad", "office_view"
| `<terminal id>`        | (string) Unique id of a Terminal Function within a specific challenge | "keypad1", "ied5"
| `<uuid>`               | (string) Universally unique identifier | "afdad684-22d4-40b1-a43d-a913f164d7fe"

## Error message and error codes

### ERROR

All messages can indicate failure using an error code (int) and error message (string). The error codes fall in the range 4000-4999 which is the range defined by RFC6455 as "for private use". This allows any client-side error handling to handle all errors, be it application or protocol errors, in the same manner.

The protocol defines at least the following errors:

| Error code | Error message |
|------------|---------------|
| `4000`     | Invalid message
| `4001`     | Client is not logged in
| `4002`     | Invalid login or password
| `4003`     | Action not allowed
| `4004`     | Object does not exist
| `4005`     | Invalid state update
| >=`4500`   | (Additional error codes as defined by Scenario)

All error messages have the following form and can follow as a response on any message. In case of error, the expected response message with header XXXXX_RESP may not be sent.

```json
S -> C: ERROR {
                "error": <error code>,
                "message": "<error message>",
                "header": "<error header>"
              }
```


## Client-Server Connection

### WELCOME

Client sets up the connection with server. Server MUST respond with the `WELCOME` message.

```json
S -> C: WELCOME {"version":"<server version>"}
```

If Client is not compatible with Server based on the provided server version, Client MUST disconnect immediately and Client SHOULD display a suitable error to the user.

### LOGIN

After a successful welcome and before performing any other Protocol Actions, Client MUST login using the `LOGIN` message. 

#### Admin login

```json
C -> S: LOGIN {
                "session": null,
                "password": "<password>"
              }
S -> C: LOGIN_RESP {
                     "connection": "<connection uuid>",
                     "session": null,
                   }
```

Password is the agreed upon Admin password.

#### Terminal login to a specific session

```json
C -> S: LOGIN {
                "session": "<session uuid>",
                "password": "<password>"
              }
S -> C: LOGIN_RESP {
                    "connection": "<connection uuid>",
                    "session": "<session uuid>",
                    "name": "<session name>",
                    "scenario": "<scenario name>",
                   }
```

Password is password set during the creation of the Session, see `SESSION_CREATE`.

* On succesful login, Server MUST respond with a `LOGIN_RESP` message.
* On failed login, Server MUST respond with an `ERROR` message with code `4002`.
* `<session uuid>` : Session that Client wants to login to.
  * When null, request login as Admin.
  * When non-null, must match a valid session.
* `<connection uuid>` : a unique identifier assigned by Server to Client to identify this specific Client.

Server must associate the Client connection with the supplied session and generated connection uuid for future interactions.

## Non-Administrative Actions

### SESSION_LIST

Listing all sessions on the Server is only allowed for Adminstrative Clients, since we do not want regular Terminals to invade any session at will.

```json
C -> S: SESSION_LIST {}
S -> C: SESSION_LIST_RESP {
                            "sessions": [
                              {
                                "session": "<session uuid>",
                                "name": "<session name>",
                                "scenario": "<scenario name>"
                              },
                              ...
                            ]
                          }
```

## Administrative Actions

All Administrative Actions can fail with regular Error Messages, and MUST be rejected by Server with error code `4003` if Client is not logged in in the Admin session, or code `4001` if Client is not logged in at all yet.

Server can filter this list at will based on the login status of Client, e.g. some sessions can be hidden based on the login password to allow for hosting multiple sites on the same Server instance.

### SESSION_CREATE
Create a new Session based on a Scenario and lock it behind the given password.

```json
C -> S: SESSION_CREATE {
                        "name": "<session name>",
                        "scenario": "<scenario name>",
                        "password": "<password>",
                        "state": {
                          (initial state) ...
                        },
                        "terminals": [
                            {
                            "terminal": "<terminal id>",
                            "function": "<terminal function>"
                            },
                            ...
                        ]
                       }
                               
S -> C: SESSION_CREATE_RESP {
                              "session": "<session uuid>"
                            }
```

### SESSION_RESET
Reset an active Session by forcing it to a specific state. This can also be used to restore a backup, as it were.

```json
C -> S: SESSION_RESET {
                        "session": "<session uuid>",
                        "state": {
                          (new state) ...
                        }
                      }
S -> C: SESSION_RESET_RESP {
                             "session": "<session uuid>"
                           }
S -> OTHER: SCENARIO_UPDATE { ... }
```

After resetting the state, Server MUST send out state updates to all connected Terminals using the `SCENARIO_UPDATE` message with the special value "RESET" in the "event" field.

### SESSION_STOP
Stop an active Session.

```json
C -> S: SESSION_STOP {
                      "session": "<session uuid>",
                     }
S -> C: SESSION_STOP_RESP {
                            "session": "<session uuid>"
                          }
S -> OTHER: SESSION_STOPPED { ... }
```

After stopping a session, Server MUST send out `SESSION_STOPPED` message to all Admin connections and all Terminals of that specific session.

### SESSION_STOPPED

Notification sent by Server to indicate a session has ended.

```json
S -> C: SESSION_STOPPED {
                          "session": "<session uuid>"
                        }
```

After sending the message, Server MUST disassociate all Terminals from that session. Terminals SHOULD disconnect upon receipt of this message unless they were Admin logins.

## Terminal Actions

### TERMINAL_LIST
List all Terminal Function of the current or specified Session.

```json
C -> S: TERMINAL_LIST {
                        "session": "<session uuid>",
                      }
S -> C: TERMINAL_LIST_RESP {
                             "session": "<session uuid>",
                             "terminals": [
                               {
                                 "terminal": "<terminal id>",
                                 "function": "<terminal function>",
                                 "connections": [
                                    "<uuid>",
                                    ...
                                 ]
                               },
                               ...
                             ]
                           }
```
The connections is the list of current connections that are attached to this terminal. See TERMINAL_ATTACH.
### TERMINAL_ATTACH
Attach to a specific Terminal Function.

Note:
Terminals are only allowed to connect to Terminal Function inside their own Session, whereas Admin connections are allowed to attach to any function in any session. This can be helpful for Admin consoles to quickly attach to check status or do status updates.


```json
C -> S: TERMINAL_ATTACH {
                          "session": "<session uuid>",
                          "terminal": "<terminal id>" | null
                        }
S -> C: TERMINAL_ATTACH_RESP {
                            "session": "<session uuid>",
                            "terminal": "<terminal id>" | null,
                            "function": "<terminal function>" | null,
                            "state": {
                              (full scenario state) ...
                            }
                          }
S -> Other: TERMINAL_EVENT { ... }
```

Attaches the Terminal to the specified Terminal Function. A Terminal can only be attached to one Terminal Function at any one time, although it is possible to request a detach by attaching to "null".

Server SHOULD send out attach/detach events using `TERMINAL_EVENT` to all Admin Clients and all Terminals in the same Session after processing the request.

### TERMINAL_EVENT
Receive attach and detach events from other Terminals

```json
S -> C: TERMINAL_EVENT {
                         "session": "<session uuid>",
                         "connection": "<connection uuid>",
                         "terminal": "<terminal id>" | null
                       }
```

## Terminal Function Actions

### SCENARIO_EVENT
Send Scenario events. The value for "event" is scenario specific, but should be considered as a description and not as a value that is to be parsed and/or interpreted. It is useful for logging, e.g. "13:05:12 Keypad solved".

```json
C -> S: SCENARIO_EVENT {
                         "session": "<session uuid>",
                         "event": "scenario-specific-event...",
                         "update": {
                           "state.selector": "new-value ...",
                           "state.selector2": { new-value2 ... },
                           ...
                         }
                       }
S -> C: SCENARIO_EVENT_RESP {
                             "session": "<session uuid>",
                             "event": "scenario-specific-event...",
                             "state": {
                              ( new-state-after-update )
                             }
                            }
S -> OTHER: SCENARIO_UPDATE { ... }
```

Update statement uses dot-indexing into the state dictionary. Given the following example structure:

```json
{
  "stage": 3,
  "keypad1": {
    "value": "1234",
    "solved": false
  },
  "ied5": {
    "locked": true,
    "somedata": "somevalue"
  }
}
```

Could be updated using the following example update event:

```json
{
  "stage": 4,
  "keypad1.solved": true,
  "ied5.locked": false
}
```

Note the following special cases:
* Adding a key that does not exist is not an error, it will just cause the entry to be added.
* Removing a key can be done by assigning `null`
* It is possible and allowed to replace entire trees in the state
* Attempting to add a key to something that is not a dictionary, i.e. accessing "ied5.somedata.subkey" in the above example, results in an error `4005`. Server SHOULD revert to the old state and undo all changes done earlier by the same event.

Once the state is successfully processed, Server MUST send a state update to all other connected Terminals using the `SCENARIO_UPDATE` message.

### SCENARIO_UPDATE
Receive Scenario events from other Terminal Functions

```json
S -> C: SCENARIO_UPDATE {
                         "session": "<session uuid>",
                         "event": "scenario-specific-event...",
                         "state": {
                          ( new-state-after-update )
                         }
                       }
```
