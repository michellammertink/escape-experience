# README

## Backend

### Run
```
cd backend
npm ci
npm run dev
```

### Tests
```
cd backend
npm ci
npm run test
```

To run only one test:
```
node --test --test-name-pattern="<name matching regex>"
```

## Frontend

### Run
```
cd frontend
npm ci
npm run dev
```

The frontend builds to the backend/dist folder. The backend needs to serve these files statically. For now both can be run in debug mode.


## Deployment

### DNS zone
```
cd infrastructuur/tf-dnszone
terraform apply --auto-approve
```

### VM 

```
cd infrastructuur/tf-vm
terraform apply --auto-approve
```

### Ansible
```
cd infrastructuur/ansible-vm
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts vm.yml
```

# Certificates

For renewal or manipulation of certificates:

Start compose in certificates folder

```
cd infrastructure/certificates
docker compose up
```

In other shell

```
docker ps
docker exec -it <containername> /bin/bash
```

Then execute:
```
certbot certonly --manual --manual-auth-hook /etc/letsencrypt/acme-dns-auth.py --preferred-challenges dns --debug-challenges -d 'cvdhacklab.nl'
```

Exit the container, then execute the move_certificates.sh file
`./move_certificates.sh`