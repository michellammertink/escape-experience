# This is a separate file because it cannot be guaranteed that azure selects the same nameservers for a domain,
# so the dnszone needs to be configured permanently, otherwise the registrar needs to be updated (which cannot
# easily be scripted)

data "azurerm_resource_group" "hacklab" {
  name = "hacklab"
}

resource "azurerm_dns_zone" "hacklab" {
  name                = "cvdhacklab.nl"
  resource_group_name = data.azurerm_resource_group.hacklab.name
}

