#!/bin/bash

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

# for vm1
DST_DIR=$SCRIPT_DIR/../ansible-vm/certificates
DST_CERT_DIR=$DST_DIR/letsencrypt/live/cvdhacklab.nl
SRC_DIR=$SCRIPT_DIR/letsencrypt/live/cvdhacklab.nl
rm -rf "${DST_DIR}"
mkdir -p "$DST_CERT_DIR"
cp "$SRC_DIR/fullchain.pem" "$DST_CERT_DIR/fullchain.pem"
cp "$SRC_DIR/privkey.pem" "$DST_CERT_DIR/privkey.pem"


