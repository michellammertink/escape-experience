terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.45.0"
    }
  }
}

provider "azurerm" {
  features {}

  subscription_id = "e1ce9dc5-c7b2-435b-b9ae-93b24b021217" # ACT - HBO-ICT -CTF
  tenant_id       = "a77b0754-fdc1-4a62-972c-8425ffbfcbd2"
  #skip_provider_registration = "true"
}

