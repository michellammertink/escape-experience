data "azurerm_resource_group" "hacklab" {
  name = "hacklab"
}

data "azurerm_dns_zone" "hacklab" {
  name                = "cvdhacklab.nl"
}

resource "azurerm_public_ip" "hacklab-vm1-ip" {
  name                = "hacklab-vm1-ip"
  resource_group_name = data.azurerm_resource_group.hacklab.name
  location            = data.azurerm_resource_group.hacklab.location
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_dns_a_record" "cvdhacklab" {
  name                = "@"
  zone_name           = data.azurerm_dns_zone.hacklab.name
  resource_group_name = data.azurerm_resource_group.hacklab.name
  ttl                 = 300
  records             = [azurerm_public_ip.hacklab-vm1-ip.ip_address]
}

resource "azurerm_virtual_network" "ctf" {
  name                = "CTFnetwork"
  address_space       = ["10.2.0.0/16"]
  location            = data.azurerm_resource_group.hacklab.location
  resource_group_name = data.azurerm_resource_group.hacklab.name
}

resource "azurerm_subnet" "internal" {
  name                 = "CTFinternal"
  resource_group_name  = data.azurerm_resource_group.hacklab.name
  virtual_network_name = azurerm_virtual_network.ctf.name
  address_prefixes     = ["10.2.0.0/16"]
}

resource "azurerm_network_interface" "hacklab-vm1-nic" {
  name                = "hacklab-vm1-nic"
  location            = data.azurerm_resource_group.hacklab.location
  resource_group_name = data.azurerm_resource_group.hacklab.name

  ip_configuration {
    name                          = "hacklab-vm1-ip"
    public_ip_address_id          = azurerm_public_ip.hacklab-vm1-ip.id
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "hacklab-vm1" {
  name                  = "hacklab-vm1"
  location              = data.azurerm_resource_group.hacklab.location
  resource_group_name   = data.azurerm_resource_group.hacklab.name
  network_interface_ids = [azurerm_network_interface.hacklab-vm1-nic.id]
  size                  = "Standard_B1s"
  #"Standard_B1ms"  see https://docs.microsoft.com/en-us/azure/virtual-machines/sizes-b-series-burstable
  admin_username        = "adminuser"

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("~/.ssh/id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }
}

resource "azurerm_network_security_group" "nsg" {
  name                = "nsg"
  location            = data.azurerm_resource_group.hacklab.location
  resource_group_name = data.azurerm_resource_group.hacklab.name

  security_rule {
    name                       = "ssh"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "https"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "http"
    priority                   = 111
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface_security_group_association" "ctfnsg-hacklab-vm1" {
  network_interface_id      = azurerm_network_interface.hacklab-vm1-nic.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}