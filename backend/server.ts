import WebSocket from "ws";
import {v4 as uuidv4} from 'uuid';

let Validator = require('jsonschema').Validator;
// todo omzetten naar https://zod.dev/
let v = new Validator();

/**
 * Messages
 */
interface Login {
    password: string;
    session: string | null;
}

let schemaLogin = {
    "id": "/Login",
    "type": "object",
    "properties": {
        "password": {"type": "string"},
        "session": {"type": ["string", "null"]},
    },
    "required": ["password", "session"],
    "additionalProperties": false
};

interface SessionCreate {
    name: string;
    scenario: string;
    password: string;
    state: Object;
    terminals: Array<SessionCreateTerminal>;
}

interface SessionCreateTerminal {
    terminal: string;
    function: string;
}

let schemaSessionCreate = {
    "id": "/SessionCreate",
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "scenario": {"type": "string"},
        "password": {"type": "string"},
        "state": {"type": "object"},
        "terminals": {
            "type": "array",
            "items": {
                "properties": {
                    "terminal": {"type": "string"},
                    "function": {"type": "string"}
                },
                "required": ["terminal", "function"]
            }
        }
    },
    "required": ["name", "scenario", "password", "state", "terminals"],
    "additionalProperties": false
};

interface TerminalList {
    session: string;
}

let schemaTerminalList = {
    "id": "/TerminalList",
    "type": "object",
    "properties": {
        "session": {"type": "string"}
    },
    "required": ["session"],
    "additionalProperties": false

};

interface SessionList {
}

let schemaSessionList = {
    "id": "/SessionList",
    "type": "object",
    "properties": {},
    "required": [],
    "additionalProperties": false
};

interface TerminalAttach {
    session: string;
    terminal: string | null;
}

let schemaTerminalAttach = {
    "id": "/TerminalAttach",
    "type": "object",
    "properties": {
        "session": {"type": "string"},
        "terminal": {"type": ["string", "null"]}
    },
    "required": ["session", "terminal"],
    "additionalProperties": false
};


interface ScenarioUpdate {
    session: string;
    event: string;
    state: { [key: string]: any };
}

let schemaScenarioEvent = {
    "id": "/ScenarioEvent",
    "type": "object",
    "properties": {
        "session": {"type": "string"},
        "event": {"type": "string"},
        "update": {"type": "object"} //todo how to specify that key is an string?
    },
    "required": ["session", "event", "update"],
    "additionalProperties": false
};

let schemaScenarioReset = {
    "id": "/ScenarioReset",
    "type": "object",
    "properties": {
        "session": {"type": "string"},
        "state": {"type": "object"} //todo how to specify that key is an string?
    },
    "required": ["session", "state"],
    "additionalProperties": false
};

interface TerminalEvent {
    session: string;
    connection: string;
    terminal: string | null;
}

interface SessionCreateTerminal {
    terminal: string;
    function: string;
}

interface TerminalListTerminal {
    terminal: string;
    function: string;
    connections: Array<string>
}

interface ScenarioEvent {
    session: string;
    update: { [index: string]: any };
    event: string
}

interface ScenarioReset {
    session: string;
    state: { [index: string]: any }
}

interface ScenarioUpdate {
    session: string;
    state: { [index: string]: any };
    event: string
}

/**
 * Internal state
 */
interface Terminal {
    id: string;
    function: string;
}

const DEBUG: boolean = true;

class Server {
    private connections: Set<Connection>;
    private sessions: Set<Session>;

    constructor() {
        this.connections = new Set();
        this.sessions = new Set();
        this.createHardcodedSession();
    }

    createHardcodedSession() {
        let state = {
            /*
             3D environment
             These are the status of various switchgear as reported by the 3d environment
             * closed: true | false; whether the switch is in closed state
             * broken: true | false; whether the switch is broken by the player
             */
            switch1: { "closed": true, "broken": false },
            switch2: { "closed": true, "broken": false },
            /*
             IED
             These are the controls that can be sent to the 3d environment by the IED to control the switches
             These events should be set to null immediately by the IED device after being set to "open" or "close" (it is a pulse)
             Values: "open" | "close" | null
             */
            switch1IEDChange: null,
            switch2IEDChange: null,
            /**
             * Keypad values: "unsolved" | "solved"
             */
            keypad: "unsolved",
            /**
             * ied.loggedIn: true | false;  Successful login
             * ied.accessed: true | false;  Webpage is viewed
             */
            ied:{
                loggedIn: false,
                accessed: false
            },
            /**
             * Whole challenge is solved, values: true | false
             */
            solved: false
        };

        let uuid = "41439156-9726-46c7-9ce6-587aa3411a5d";
        let name = "sessionname";
        let password = "mypass";
        let scenario = "a";
        const session = new Session(uuid, name, password, scenario, state, []);

        this.addSession(session);
    }

    addClient(ws: WebSocket) {
        this.connections.add(new Connection(this, ws))
    }

    addSession(session: Session) {
        this.sessions.add(session);
    }

    getSession(sessionUUID: string | null) {
        for (let session of this.sessions.values()) {
            if (session.getUuid() === sessionUUID) {
                return session;
            }
        }
        return null;
    }

    getSessionList() {
        let list = []; //todo fix any
        for (let session of this.sessions.values()) {
            let obj = {
                session: session.getUuid(),
                name: session.getName(),
                scenario: session.getScenario()
            }
            list.push(obj);
        }
        return {sessions: list};
    }

    sendTerminalEventToOthersInSession(exclude: Connection, session: Session, event: TerminalEvent) {
        for (let connection of this.connections) {
            if (connection !== exclude && connection.hasSession(session)) {
                connection.sendMessage("TERMINAL_EVENT", event);
            }
        }
    }

    getConnectionUUIDsForTerminal(terminal: Terminal) {
        let result: string[] = [];
        for (let connection of this.connections) {
            if (connection.hasTerminal(terminal)) {
                result.push(connection.getUUID());
            }
        }
        return result;
    }

    sendScenarioUpdateToOthersInSession(exclude: Connection, session: Session, event: ScenarioUpdate) {
        for (let connection of this.connections) {
            //if (connection !== exclude && connection.hasSession(session)) { todo fix in protocol, everyone gets state update
            if (connection.hasSession(session)) {
                connection.sendMessage("SCENARIO_UPDATE", event);
            }
        }
    }
}

class Connection {
    private readonly websocket: WebSocket;
    private server: Server;
    private readonly uuid: string;
    private session: Session | null | "admin";
    private terminal: Terminal | null;

    private PASSWORD: string = "7fd8603c-36c9-47c1-aeb3-665a1ef233ec";

    /**
     * @param server
     * @param ws
     */
    constructor(server: Server, ws: WebSocket) {
        this.websocket = ws;
        this.server = server;
        this.uuid = uuidv4();
        this.session = null;
        this.terminal = null;

        ws.on('message', (msg) => {
            this.#handleMessage(msg.toString());
        });

        ws.on('close', (msg) => {
            this.#handleClose();
        });

        ws.on('error', () => {
            this.#handleError();
        });

        this.sendMessage("WELCOME", {version: process.env.VERSION})
    }

    sendMessage(message: String, object: Object) {
        if (DEBUG) console.log(this.uuid.substring(0, 8) + " <- " + message + " " + JSON.stringify(object));
        this.websocket.send(message + " " + JSON.stringify(object))
    }

    #handleMessage(message: String) {
        let [header, ...dataArray] = message.split(' ');
        let data = dataArray.join(' ');

        if (DEBUG) console.log(this.uuid.substring(0, 8) + " -> " + header + " " + data);
        switch (header) {
            case "LOGIN":
                let login: Login = JSON.parse(data);
                this.#processLogin(login);
                break;
            case "SESSION_CREATE":
                let sessionCreate: SessionCreate = JSON.parse(data);
                this.#processSessionCreate(sessionCreate);
                break;
            case "TERMINAL_LIST":
                let terminalList: TerminalList = JSON.parse(data);
                this.#processTerminalList(terminalList);
                break;
            case "SESSION_LIST":
                let sessionList: SessionList = JSON.parse(data);
                this.#processSessionList(sessionList);
                break;
            case "TERMINAL_ATTACH":
                let terminalAttach: TerminalAttach = JSON.parse(data);
                this.#processTerminalAttach(terminalAttach);
                break;
            case "SCENARIO_EVENT":
                let scenarioEvent: ScenarioEvent = JSON.parse(data);
                this.#processScenarioEvent(scenarioEvent);
                break;
            case "SCENARIO_RESET":
                let scenarioReset: ScenarioReset = JSON.parse(data);
                this.#processScenarioReset(scenarioReset);
                break;
            default:
                //Ignore everything else
                break;
        }
    }

    #handleError() {
        //TODO clear administration
    }

    #handleClose() {

    }

    #processLogin(data: Login) {
        try {
            this.#ensureValid("LOGIN", data, schemaLogin)
            this.session = null;
            if (data.session === null) {
                this.#processAdminLogin(data);
            } else {
                this.#processSessionLogin(data);
            }
        } catch (e) {
            //nothing to do
        }
    }

    #processSessionLogin(data: Login) {
        this.session = this.server.getSession(data.session);
        if (this.session != null && this.session.getPassword() === data.password) {
            this.sendMessage("LOGIN_RESP", {
                connection: this.uuid,
                session: this.session.getUuid(),
                name: this.session.getName(),
                scenario: this.session.getScenario(),
                state: this.session.getState() //todo update in doc
            })
        } else {
            this.sendMessage("ERROR", {
                error: 4002,
                message: "Invalid password or sessionid",
                header: "LOGIN"
            })
        }
    }

    #processAdminLogin(data: Login) {
        if (data.password === this.PASSWORD) {
            this.session = "admin";
            this.sendMessage("LOGIN_RESP", {
                connection: this.uuid,
                session: null, //indicates admin session
            })
        } else {
            this.sendMessage("ERROR", {
                error: 4002,
                message: "Invalid password",
                header: "LOGIN"
            })
        }
    }

    #processSessionCreate(data: SessionCreate) {
        try {
            this.#ensureValid("SESSION_CREATE", data, schemaSessionCreate)
            this.#ensureLoggedIn("SESSION_CREATE");
            this.#ensureAdmin("SESSION_CREATE");

            let terminals: Array<Terminal> = [];
            for (let inputTerminal of data.terminals) {
                let terminal: Terminal = {
                    id: inputTerminal.terminal,
                    function: inputTerminal.function
                }
                terminals.push(terminal);
            }
            const session = new Session(uuidv4(), data.name, data.password,
                data.scenario, data.state, terminals);

            this.server.addSession(session);
            this.sendMessage("SESSION_CREATE_RESP", {
                session: session.getUuid()
            })
        } catch (e) {
            //nothing to do
        }
    }

    #processSessionList(data: SessionList) {
        try {
            this.#ensureValid("SESSION_LIST", data, schemaSessionList)
            //this.#ensureLoggedIn("SESSION_LIST");
            //this.#ensureAdmin("SESSION_LIST");

            this.sendMessage("SESSION_LIST_RESP", this.server.getSessionList());
        } catch (e) {
            //nothing to do
        }
    }

    #processTerminalList(terminalList: TerminalList) {
        try {
            this.#ensureValid("TERMINAL_LIST", terminalList, schemaTerminalList)
            this.#ensureLoggedIn("TERMINAL_LIST");
            let session = this.#ensureSession("TERMINAL_LIST", terminalList.session);
            this.#ensureAccessAllowed("TERMINAL_LIST", terminalList.session);

            let terminals = session.getTerminals();
            let totalTerminalList: TerminalListTerminal[] = [];
            for (let terminal of terminals) {
                totalTerminalList.push({
                    "terminal": terminal.id,
                    "function": terminal.function,
                    "connections": this.server.getConnectionUUIDsForTerminal(terminal)
                });
            }

            this.sendMessage("TERMINAL_LIST_RESP", {
                session: session.getUuid(),
                terminals: totalTerminalList
            });
        } catch (e) {
            //nothing to do
        }
    }

    #sendTerminalEventToOthers(session: Session, terminal: Terminal) {
        this.server.sendTerminalEventToOthersInSession(this, session, {
            "session": session.getUuid(),
            "connection": this.uuid,
            "terminal": terminal.id
        });
    }

    #processTerminalAttach(terminalAttach: TerminalAttach) {
        try {
            this.#ensureValid("TERMINAL_ATTACH", terminalAttach, schemaTerminalAttach)
            this.#ensureLoggedIn("TERMINAL_ATTACH");
            let session = this.#ensureSession("TERMINAL_ATTACH", terminalAttach.session);
            this.#ensureAccessAllowed("TERMINAL_ATTACH", terminalAttach.session);

            let term = session.attachConnectionToTerminal(this, terminalAttach.terminal);
            if (terminalAttach.terminal !== null && term !== null) {
                // Want to assign something and it succeeded
                this.sendMessage("TERMINAL_ATTACH_RESP", {
                    session: session.getUuid(),
                    terminal: term.id,
                    function: term.function,
                    state: session.getState()
                });
                this.#sendTerminalEventToOthers(session, term);

            } else if (terminalAttach.terminal === null) {
                // Did not want to assign something (detach)
                this.sendMessage("TERMINAL_ATTACH_RESP", {
                    session: session.getUuid(),
                    terminal: null,
                    function: null,
                    state: session.getState()
                });

                //todo detach event
            } else {
                this.sendMessage("ERROR", {
                    error: 4004,
                    message: "Object does not exist",
                    header: "TERMINAL_ATTACH"
                })
            }
        } catch (e) {
            // nothing to do
        }
    }

    #processScenarioEvent(scenarioEvent: ScenarioEvent) {
        try {
            this.#ensureValid("SCENARIO_EVENT", scenarioEvent, schemaScenarioEvent)
            this.#ensureLoggedIn("SCENARIO_EVENT");
            let session = this.#ensureSession("SCENARIO_EVENT", scenarioEvent.session);
            this.#ensureAccessAllowed("SCENARIO_EVENT", scenarioEvent.session);

            //todo do something with scenarioEvent.event
            session.mergeStateUpdate(scenarioEvent.update);

            this.sendMessage("SCENARIO_EVENT_RESP", {
                session: session.getUuid(),
                event: scenarioEvent.event,
                state: session.getState()
            })

            this.#sendScenarioUpdateToOthers(session, scenarioEvent.event);

        } catch (e) {
            // nothing to do
        }
    }

    #processScenarioReset(scenarioReset: ScenarioReset) {
        try {
            this.#ensureValid("SCENARIO_RESET", scenarioReset, schemaScenarioReset)
            this.#ensureLoggedIn("SCENARIO_RESET");
            let session = this.#ensureSession("SCENARIO_RESET", scenarioReset.session);
            this.#ensureAccessAllowed("SCENARIO_RESET", scenarioReset.session);

            session.setState(scenarioReset.state);

            this.sendMessage("SCENARIO_RESET_RESP", {
                session: session.getUuid()
            })

            this.#sendScenarioUpdateToOthers(session, "RESET");

        } catch (e) {
            // nothing to do
        }
    }

    #ensureValid(header: string, obj: Object, schema: Object) {
        let validatorResult = v.validate(obj, schema);
        if (!validatorResult.valid) {
            this.sendMessage("ERROR", {
                error: 4000,
                message: "Invalid JSON",
                header: header
            })
            throw new Error("invalid")
        }
    }

    #ensureSession(header: string, sessionString: string) {
        let session = this.server.getSession(sessionString);
        if (session === null) {
            this.sendMessage("ERROR", {
                error: 4004,
                message: "Object does not exist",
                header: header
            })
            throw new Error("no valid session found")
        }
        return session;
    }

    #ensureAccessAllowed(header: string, session: string) {
        if (this.session === null || (this.session !== "admin" &&
            this.session.getUuid() !== session)) {
            this.sendMessage("ERROR", {
                error: 4003,
                message: "Action is not allowed",
                header: header
            })
            throw new Error("no access")
        }
    }

    #ensureLoggedIn(header: string,) {
        if (this.session === null) {
            this.sendMessage("ERROR", {
                error: 4001,
                message: "Connection is not logged in",
                header: header
            })
            throw new Error("not logged in");
        }

    }

    #ensureAdmin(header: string,) {
        if (this.session !== "admin") {
            this.sendMessage("ERROR", {
                error: 4003,
                message: "Action not allowed",
                header: header
            })
            throw new Error("not admin");
        }

    }

    setTerminal(terminal: Terminal | null) {
        this.terminal = terminal;
    }

    hasTerminal(terminal: Terminal) {
        return this.terminal === terminal;
    }

    getUUID() {
        return this.uuid;
    }

    hasSession(session: Session) {
        return this.session == session;
    }

    #sendScenarioUpdateToOthers(session: Session, event: string) {
        this.server.sendScenarioUpdateToOthersInSession(this, session, {
            session: session.getUuid(),
            event: event,
            state: session.getState()
        });
    }
}

class Session {
    private uuid: string;
    private name: string;
    private password: string;
    private scenario: string;
    private state: { [key: string]: any };
    private terminals: Array<Terminal>;

    constructor(uuid: string, name: string, password: string, scenario: string, state: Object, terminals: Array<Terminal>) {
        this.uuid = uuid;
        this.name = name;
        this.password = password;
        this.scenario = scenario;
        this.state = state;
        this.terminals = terminals;
    }

    getName() {
        return this.name;
    }

    getUuid() {
        return this.uuid;
    }

    getScenario() {
        return this.scenario;
    }

    getPassword() {
        return this.password;
    }

    getTerminals() {
        return this.terminals;
    }

    /**
     * Make a connection between this connection and this session terminal id (e.g. numpad1)
     * If terminal_id is null the terminal of the connection should be set to null (detach)
     * @param connection
     * @param terminal_id
     */
    attachConnectionToTerminal(connection: Connection, terminal_id: string | null) {
        let terminal: Terminal | null = null;
        for (let term of this.terminals) {
            if (term.id === terminal_id) {
                terminal = term;
            }
        }
        connection.setTerminal(terminal);
        return terminal;
    }

    getState() {
        return this.state;
    }

    setState(newState: { [x: string]: any; }) {
        this.state = newState;
    }

    mergeStateUpdate(update: { [index: string]: any }) {
        for (let key in update) { //todo test empty object!
            this.state[key] = update[key];
        }
    }
}

export {Server}
