import {afterEach, before, beforeEach, describe, it} from "node:test";
import WebSocket from 'ws';
import assert from "node:assert";

describe('With no existing sessions', function () {
    it('Welcome message - should be received on connect', async function () {
        return new Promise((resolve, reject) => {
            const ws = new WebSocket('ws://localhost/ws')
            //ws.on('open', () => console.log('Client connected'));

            ws.on('message', (data) => {
                if (data.toString() === 'WELCOME {"version":"1.0.0"}') {
                    ws.close();
                    resolve();
                } else {
                    ws.close();
                    reject("Wrong welcome message");
                }
            });
            setTimeout(() => {
                ws.close();
                reject("Timeout on welcome message");
            }, 1000)
        });
    });

    it('Login - as admin with wrong password - should fail', async function () {
        return new Promise((resolve, reject) => {
            const ws = new WebSocket('ws://localhost/ws')
            //ws.on('open', () => console.log('Client connected'));

            ws.on('message', (data) => {
                let message = data.toString();

                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        ws.send("LOGIN " + JSON.stringify({session: null, password: "a"}))
                        break;
                    case 'ERROR':
                        if (content.error === 4002) {
                            ws.close();
                            resolve();
                        } else {
                            reject("Wrong error code")
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                ws.close();
                reject("Timeout");
            }, 1000)
        });
    });


    it('Login - invalid json - should fail', async function () {
        return new Promise((resolve, reject) => {
            const ws = new WebSocket('ws://localhost/ws')
            //ws.on('open', () => console.log('Client connected'));

            ws.on('message', (data) => {
                let message = data.toString();

                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        ws.send("LOGIN " + JSON.stringify({sessWRONGion: null, password: "a"}))
                        break;
                    case 'ERROR':
                        if (content.error === 4000) {
                            ws.close();
                            resolve();
                        } else {
                            ws.close();
                            reject("Wrong error code")
                        }
                        break;
                    default:
                        ws.close();
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                ws.close();
                reject("Timeout");
            }, 1000)
        });
    });

    it('Login - as admin with correct password - should succeed', async function () {
        return new Promise((resolve, reject) => {
            const ws = new WebSocket('ws://localhost/ws')
            //ws.on('open', () => console.log('Client connected'));

            ws.on('message', (data) => {
                let message = data.toString();

                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        ws.send("LOGIN " + JSON.stringify({
                            session: null,
                            password: "7fd8603c-36c9-47c1-aeb3-665a1ef233ec"
                        }))
                        break;
                    case 'LOGIN_RESP':
                        if (content.session === null &&
                            content.connection.length === 36) {
                            ws.close();
                            resolve();
                        } else {
                            ws.close();
                            reject("Session should be null or wrong uuid length")
                        }
                        break;
                    default:
                        ws.close();
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                ws.close();
                reject("Timeout");
            }, 1000)
        });
    });

    it('Create session - not logged in - should fail', async function () {
        return new Promise((resolve, reject) => {
            const ws = new WebSocket('ws://localhost/ws')
            //ws.on('open', () => console.log('Client connected'));

            ws.on('message', (data) => {
                let message = data.toString();

                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        ws.send("SESSION_CREATE " + JSON.stringify({
                            name: "test",
                            scenario: "test",
                            password: "test",
                            state: {},
                            terminals: []
                        }))
                        break;
                    case 'ERROR':
                        if (content.error === 4001) {
                            ws.close();
                            resolve();
                        } else {
                            ws.close();
                            reject("Wrong error returned")
                        }
                        break;
                    default:
                        ws.close();
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                ws.close();
                reject("Timeout");
            }, 1000)
        });
    });

    it('Create session - as admin - should succeed', async function () {
        return new Promise((resolve, reject) => {
            const ws = new WebSocket('ws://localhost/ws')
            //ws.on('open', () => console.log('Client connected'));

            ws.on('message', (data) => {
                let message = data.toString();

                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        ws.send("LOGIN " + JSON.stringify({
                            session: null,
                            password: "7fd8603c-36c9-47c1-aeb3-665a1ef233ec"
                        }))
                        break;
                    case 'LOGIN_RESP':
                        ws.send("SESSION_CREATE " + JSON.stringify({
                            name: "test",
                            scenario: "test",
                            password: "test",
                            state: {},
                            terminals: []
                        }))
                        break;
                    case 'SESSION_CREATE_RESP':
                        if (content.session.length === 36) {
                            ws.close();
                            resolve();
                        } else {
                            ws.close();
                            reject("Wrong session uuid length")
                        }
                        break;
                    default:
                        ws.close();
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                ws.close();
                reject("Timeout");
            }, 1000)
        });
    });


    it('Create session - as admin with invalid json - should fail', async function () {
        return new Promise((resolve, reject) => {
            const ws = new WebSocket('ws://localhost/ws')
            //ws.on('open', () => console.log('Client connected'));

            ws.on('message', (data) => {
                let message = data.toString();

                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        ws.send("LOGIN " + JSON.stringify({
                            session: null,
                            password: "7fd8603c-36c9-47c1-aeb3-665a1ef233ec"
                        }))
                        break;
                    case 'LOGIN_RESP':
                        ws.send("SESSION_CREATE " + JSON.stringify({
                            name: "test",
                            scenario: "test",
                            password: "test",
                            state: {},
                            terminTHISISNOTCORRECTals: []
                        }))
                        break;
                    case 'ERROR':
                        if (content.error === 4000) {
                            ws.close();
                            resolve();
                        } else {
                            ws.close();
                            reject("Wrong error code")
                        }
                        break;
                    default:
                        ws.close();
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                ws.close();
                reject("Timeout");
            }, 1000)
        });
    });
})

describe('With one connection', function () {
    let ws = null;
    let createdSessionUUID = "";

    // login as admin and create session
    beforeEach(async function () {
        ws = new WebSocket('ws://localhost/ws')

        return new Promise((resolve, reject) => {
            ws.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        ws.send("LOGIN " + JSON.stringify({
                            session: null,
                            password: "7fd8603c-36c9-47c1-aeb3-665a1ef233ec"
                        }))
                        break;
                    case 'LOGIN_RESP':
                        assert.strictEqual(content.session, null)
                        ws.send("SESSION_CREATE " + JSON.stringify({
                            name: "test",
                            scenario: "testScenario",
                            password: "mySecretPassword",
                            state: {},
                            terminals: [
                                {
                                    "terminal": "tablet1",
                                    "function": "numpad"
                                },
                                {
                                    "terminal": "tablet2",
                                    "function": "3dmodel"
                                }
                            ]
                        }))
                        break;
                    case 'SESSION_CREATE_RESP':
                        createdSessionUUID = content.session;
                        ws.removeAllListeners("message");
                        resolve();
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });

    });

    afterEach(async function () {
        ws.close();
    });

    it('Login - as session with valid password - should succeed', async function () {
        return new Promise((resolve, reject) => {

            ws.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'LOGIN_RESP':
                        if (content.connection.length === 36
                            && content.session === createdSessionUUID
                            && content.name === "test"
                            && content.scenario === "testScenario") {
                            resolve();
                        } else {
                            reject("Wrong session login response " + message + ">" + createdSessionUUID)
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            ws.send("LOGIN " + JSON.stringify({
                session: createdSessionUUID,
                password: "mySecretPassword"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Login - as session with wrong password - should fail', async function () {
        return new Promise((resolve, reject) => {

            ws.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'ERROR':
                        if (content.error === 4002) {
                            resolve();
                        } else {
                            reject("Wrong error code returned")
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            ws.send("LOGIN " + JSON.stringify({
                session: createdSessionUUID,
                password: "mySecretWrongPassword"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });
})

describe('With multiple connections, two sessions', function () {
    let wsAdmin1Session1 = null;
    let wsAdmin2Session2 = null;
    let wsSession1Connection1 = null;
    let wsSession1Connection2 = null;
    let wsConnection3 = null; //not logged in

    let session1UUID = "";
    let session2UUID = "";
    let connection1UUID = "";
    let connection2UUID = "";
    let wsAdmin1ConnectionUUID = "";
    let wsAdmin2ConnectionUUID = "";

    // login as admin and create session
    beforeEach(async function () {
        wsAdmin1Session1 = new WebSocket('ws://localhost/ws')

        return new Promise((resolve, reject) => {
            wsAdmin1Session1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        wsAdmin1Session1.send("LOGIN " + JSON.stringify({
                            session: null,
                            password: "7fd8603c-36c9-47c1-aeb3-665a1ef233ec"
                        }))
                        break;
                    case 'LOGIN_RESP':
                        assert.strictEqual(content.session, null)
                        wsAdmin1ConnectionUUID = content.connection;
                        wsAdmin1Session1.send("SESSION_CREATE " + JSON.stringify({
                            name: "myTest",
                            scenario: "myTestScenario",
                            password: "mySecretPassword",
                            state: {
                                numpad1: {solved: false},
                                numpad9: {other: 1}
                            },
                            terminals: [
                                {
                                    "terminal": "tablet1",
                                    "function": "numpad"
                                },
                                {
                                    "terminal": "tablet2",
                                    "function": "3dmodel"
                                }
                            ]
                        }))
                        break;
                    case 'SESSION_CREATE_RESP':
                        session1UUID = content.session;
                        wsAdmin1Session1.removeAllListeners("message");
                        resolve();
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    beforeEach(async function () {
        wsAdmin2Session2 = new WebSocket('ws://localhost/ws')

        return new Promise((resolve, reject) => {
            wsAdmin2Session2.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        wsAdmin2Session2.send("LOGIN " + JSON.stringify({
                            session: null,
                            password: "7fd8603c-36c9-47c1-aeb3-665a1ef233ec"
                        }))
                        break;
                    case 'LOGIN_RESP':
                        assert.strictEqual(content.session, null)
                        wsAdmin2ConnectionUUID = content.connection;
                        wsAdmin2Session2.send("SESSION_CREATE " + JSON.stringify({
                            name: "myOtherTest",
                            scenario: "myOtherTestScenario",
                            password: "myOtherSecretPassword",
                            state: {
                                numpad1: {solved: false},
                                numpad9: {other: 1}
                            },
                            terminals: [
                                {
                                    "terminal": "tablet1",
                                    "function": "numpad"
                                },
                                {
                                    "terminal": "tablet2",
                                    "function": "3dmodel"
                                }
                            ]
                        }))
                        break;
                    case 'SESSION_CREATE_RESP':
                        session2UUID = content.session;
                        wsAdmin2Session2.removeAllListeners("message");
                        resolve();
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });

    });

    beforeEach(async function () {
        wsSession1Connection1 = new WebSocket('ws://localhost/ws')

        return new Promise((resolve, reject) => {
            wsSession1Connection1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        wsSession1Connection1.send("LOGIN " + JSON.stringify({
                            session: session1UUID,
                            password: "mySecretPassword"
                        }))
                        break;
                    case 'LOGIN_RESP':
                        assert.strictEqual(content.session, session1UUID)
                        connection1UUID = content.connection;
                        wsSession1Connection1.removeAllListeners("message");
                        resolve();
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });

    });

    beforeEach(async function () {
        wsSession1Connection2 = new WebSocket('ws://localhost/ws')

        return new Promise((resolve, reject) => {
            wsSession1Connection2.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        wsSession1Connection2.send("LOGIN " + JSON.stringify({
                            session: session1UUID,
                            password: "mySecretPassword"
                        }))
                        break;
                    case 'LOGIN_RESP':
                        assert.strictEqual(content.session, session1UUID)
                        connection2UUID = content.connection;
                        wsSession1Connection2.removeAllListeners("message");
                        resolve();
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    beforeEach(async function () {
        wsConnection3 = new WebSocket('ws://localhost/ws')

        return new Promise((resolve, reject) => {
            wsConnection3.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                //content = JSON.parse(content.join(' '));

                switch (header) {
                    case 'WELCOME':
                        wsConnection3.removeAllListeners("message");
                        resolve();
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });

    });

    afterEach(async function () {
        wsAdmin1Session1.close();
        wsAdmin2Session2.close();
        wsSession1Connection1.close();
        wsSession1Connection2.close();
        wsConnection3.close();
    });

    it('Session list - as admin - should succeed', async function () {
        return new Promise((resolve, reject) => {

            wsAdmin1Session1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'SESSION_LIST_RESP':
                        let sessions = content.sessions;
                        for (let session of sessions) {
                            if (session.name === "myTest"
                                && session.scenario === "myTestScenario"
                                && session.session === session1UUID) {
                                resolve();
                            }
                        }
                        reject("Session which was created is not found")
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsAdmin1Session1.send("SESSION_LIST " + JSON.stringify({}))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Session list - not logged in - should succeed', async function () {
        return new Promise((resolve, reject) => {

            wsConnection3.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'SESSION_LIST_RESP':
                        let sessions = content.sessions;
                        for (let session of sessions) {
                            if (session.name === "myTest"
                                && session.scenario === "myTestScenario"
                                && session.session === session1UUID) {
                                resolve();
                            }
                        }
                        reject("Session which was created is not found")
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsConnection3.send("SESSION_LIST " + JSON.stringify({}))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });


    it('Session list - wrong json - should fail', async function () {
        return new Promise((resolve, reject) => {

            wsAdmin1Session1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'ERROR':
                        if (content.error === 4000) {
                            resolve();
                        } else {
                            reject("Wrong error code")
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsAdmin1Session1.send("SESSION_LIST " + JSON.stringify({unexpected: ""}))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Terminal list - admin - should succeed', async function () {
        return new Promise((resolve, reject) => {

            wsAdmin1Session1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'TERMINAL_LIST_RESP':
                        if (content.session === session1UUID
                            && content.terminals[0].terminal === "tablet1"
                            && content.terminals[0].function === "numpad"
                            && content.terminals[1].terminal === "tablet2"
                            && content.terminals[1].function === "3dmodel"
                            && content.terminals[0].connections
                            && content.terminals[1].connections
                            && content.terminals[0].connections.length === 0
                            && content.terminals[1].connections.length === 0) {
                            resolve();
                        } else {
                            reject("Wrong terminal state");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsAdmin1Session1.send("TERMINAL_LIST " + JSON.stringify({
                session: session1UUID
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Terminal list - not logged in - should fail', async function () {
        return new Promise((resolve, reject) => {

            wsConnection3.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'ERROR':
                        if (content.error === 4001) {
                            resolve();
                        } else {
                            reject("Wrong error code");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsConnection3.send("TERMINAL_LIST " + JSON.stringify({
                session: session1UUID
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Terminal list - wrong json - should fail', async function () {
        return new Promise((resolve, reject) => {

            wsAdmin1Session1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'ERROR':
                        if (content.error === 4000) {
                            resolve();
                        } else {
                            reject("Wrong error code");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsAdmin1Session1.send("TERMINAL_LIST " + JSON.stringify({
                sessWRONGion: session1UUID
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Terminal list - wrong session id - should fail', async function () {
        return new Promise((resolve, reject) => {

            wsAdmin1Session1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'ERROR':
                        if (content.error === 4004) {
                            resolve();
                        } else {
                            reject("Wrong error code");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsAdmin1Session1.send("TERMINAL_LIST " + JSON.stringify({
                session: session1UUID + "wrong"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Terminal list - as session - should succeed', async function () {
        return new Promise((resolve, reject) => {

            wsSession1Connection1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'TERMINAL_LIST_RESP':
                        if (content.session === session1UUID
                            && content.terminals[0].terminal === "tablet1"
                            && content.terminals[0].function === "numpad"
                            && content.terminals[1].terminal === "tablet2"
                            && content.terminals[1].function === "3dmodel"
                            && content.terminals[0].connections
                            && content.terminals[1].connections
                            && content.terminals[0].connections.length === 0
                            && content.terminals[1].connections.length === 0) {
                            resolve();
                        } else {
                            reject("Wrong terminal state");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsSession1Connection1.send("TERMINAL_LIST " + JSON.stringify({
                session: session1UUID
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Terminal list - as session with wrong session id - should fail', async function () {
        return new Promise((resolve, reject) => {

            wsSession1Connection1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'ERROR':
                        if (content.error === 4004) {
                            resolve();
                        } else {
                            reject("Wrong error code");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsSession1Connection1.send("TERMINAL_LIST " + JSON.stringify({
                session: session1UUID + "doesnotexist"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Terminal list - as session with wrong client - should fail', async function () {
        return new Promise((resolve, reject) => {

            wsSession1Connection1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'ERROR':
                        if (content.error === 4003) {
                            resolve();
                        } else {
                            reject("Wrong error code");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsSession1Connection1.send("TERMINAL_LIST " + JSON.stringify({
                session: session2UUID
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });


    it('Terminal attach - not logged in - should fail', async function () {
        return new Promise((resolve, reject) => {

            wsConnection3.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'ERROR':
                        if (content.error === 4001) {
                            resolve();
                        } else {
                            reject("Wrong error code");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsConnection3.send("TERMINAL_ATTACH " + JSON.stringify({
                session: session1UUID,
                terminal: "tablet1"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Terminal attach - as session - should succeed', async function () {
        let conn1promise = new Promise((resolve, reject) => {
            wsSession1Connection1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'TERMINAL_ATTACH_RESP':
                        if (content.session === session1UUID
                            && content.terminal === "tablet1"
                            && content.function === "numpad"
                            && content.state.numpad1.solved === false) {
                            resolve();
                        } else {
                            reject("Wrong terminal attach response");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsSession1Connection1.send("TERMINAL_ATTACH " + JSON.stringify({
                session: session1UUID,
                terminal: "tablet1"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)

        });

        let conn2promise = new Promise((resolve, reject) => {
            wsSession1Connection2.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'TERMINAL_EVENT':
                        if (content.session === session1UUID
                            && content.connection === connection1UUID
                            && content.terminal === "tablet1") {
                            resolve();
                        } else {
                            reject("Wrong terminal event response");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout");
            }, 1000)

        });
        return Promise.all([conn1promise, conn2promise]);
    });

    it('Terminal attach - as admin - should succeed - (please note an admin cannot force a terminal attach for another connection)', async function () {
        return new Promise((resolve, reject) => {

            wsAdmin1Session1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'TERMINAL_ATTACH_RESP':
                        if (content.session === session1UUID
                            && content.terminal === "tablet1"
                            && content.function === "numpad"
                            && content.state.numpad1.solved === false) {
                            resolve();
                        } else {
                            reject("Wrong terminal attach response");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsAdmin1Session1.send("TERMINAL_ATTACH " + JSON.stringify({
                session: session1UUID,
                terminal: "tablet1"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Terminal attach - as session - should succeed', async function () {
        let conn1promise = new Promise((resolve, reject) => {
            wsSession1Connection1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'TERMINAL_ATTACH_RESP':
                        if (content.session === session1UUID
                            && content.terminal === "tablet1"
                            && content.function === "numpad"
                            && content.state.numpad1.solved === false) {
                            resolve();
                        } else {
                            reject("Wrong terminal attach response");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsSession1Connection1.send("TERMINAL_ATTACH " + JSON.stringify({
                session: session1UUID,
                terminal: "tablet1"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)

        });

        let conn2promise = new Promise((resolve, reject) => {
            wsSession1Connection2.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'TERMINAL_EVENT':
                        if (content.session === session1UUID
                            && content.connection === connection1UUID
                            && content.terminal === "tablet1") {
                            resolve();
                        } else {
                            reject("Wrong terminal event response");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout");
            }, 1000)

        });
        return Promise.all([conn1promise, conn2promise]);
    });

    it('Terminal attach - as admin - should succeed', async function () {
        let conn1promiseAdmin = new Promise((resolve, reject) => {

            // This admin did not create the session but should still have the rights to attach a terminal to it
            // both non admin connections should receive the update

            wsAdmin2Session2.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'TERMINAL_ATTACH_RESP':
                        if (content.session === session1UUID
                            && content.terminal === "tablet1"
                            && content.function === "numpad"
                            && content.state.numpad1.solved === false) {
                            resolve();
                        } else {
                            reject("Wrong terminal attach response");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsAdmin2Session2.send("TERMINAL_ATTACH " + JSON.stringify({
                session: session1UUID,
                terminal: "tablet1"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)

        });

        let conn2promise = new Promise((resolve, reject) => {
            wsSession1Connection2.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'TERMINAL_EVENT':
                        if (content.session === session1UUID
                            && content.connection === wsAdmin2ConnectionUUID
                            && content.terminal === "tablet1") {
                            resolve();
                        } else {
                            reject("Wrong terminal event response");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout");
            }, 1000)

        });

        let conn1promise = new Promise((resolve, reject) => {
            wsSession1Connection1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'TERMINAL_EVENT':
                        if (content.session === session1UUID
                            && content.connection === wsAdmin2ConnectionUUID
                            && content.terminal === "tablet1") {
                            resolve();
                        } else {
                            reject("Wrong terminal event response");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout");
            }, 1000)

        });
        return Promise.all([conn1promiseAdmin, conn1promise, conn2promise]);
    });


    it('Terminal attach - as admin with invalid terminal - should fail', async function () {
        return new Promise((resolve, reject) => {

            wsAdmin1Session1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'ERROR':
                        if (content.error === 4004) {
                            resolve();
                        } else {
                            reject("Wrong error code");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsAdmin1Session1.send("TERMINAL_ATTACH " + JSON.stringify({
                session: session1UUID,
                terminal: "tablet1INVALID"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Terminal attach - as session with invalid session - should fail', async function () {
        return new Promise((resolve, reject) => {

            wsSession1Connection1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'ERROR':
                        if (content.error === 4004) {
                            resolve();
                        } else {
                            reject("Wrong error code");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsSession1Connection1.send("TERMINAL_ATTACH " + JSON.stringify({
                session: session1UUID + "invalid",
                terminal: "tablet1"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });


    it('Terminal attach - as admin with invalid session - should fail', async function () {
        return new Promise((resolve, reject) => {

            wsAdmin1Session1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'ERROR':
                        if (content.error === 4004) {
                            resolve();
                        } else {
                            reject("Wrong error code");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            wsAdmin1Session1.send("TERMINAL_ATTACH " + JSON.stringify({
                session: session1UUID + "invalid",
                terminal: "tablet1"
            }))

            setTimeout(() => {
                reject("Timeout");
            }, 1000)
        });
    });

    it('Terminal attach - as session with terminal is null - should succeed', async function () {
        return new Promise((resolve, reject) => {

                let attach_resp_count = 0;
                wsSession1Connection1.on('message', (data) => {
                        let message = data.toString();
                        let [header, ...content] = message.split(' ');
                        content = JSON.parse(content.join(' '));
                        switch (header) {
                            case 'TERMINAL_ATTACH_RESP':
                                attach_resp_count++;
                                if (attach_resp_count === 1) {
                                    if (content.session !== session1UUID
                                        || content.terminal !== "tablet1"
                                        || content.function !== "numpad"
                                        || content.state.numpad1.solved !== false) {
                                        throw new Error("Invalid response");
                                    }
                                    //step 2: detach
                                    wsSession1Connection1.send("TERMINAL_ATTACH " + JSON.stringify({
                                        session: session1UUID,
                                        terminal: null
                                    }))
                                } else { // second response (on detach)
                                    if (content.session === session1UUID
                                        && content.terminal === null
                                        && content.function === null
                                        && content.state.numpad1.solved === false) {
                                        resolve();
                                    } else {
                                        reject("Wrong terminal attach response");
                                    }
                                }
                                break;
                            default:
                                reject("Wrong message received: " + header)
                                break;
                        }
                    }
                );
                //step 1: attach
                wsSession1Connection1.send("TERMINAL_ATTACH " + JSON.stringify({
                    session: session1UUID,
                    terminal: "tablet1"
                }))

                setTimeout(() => {
                    reject("Timeout");
                }, 1000)
            }
        );
    });

    it('Terminal attach - as admin with terminal is null - should succeed', async function () {
        return new Promise((resolve, reject) => {

                let attach_resp_count = 0;
                wsAdmin1Session1.on('message', (data) => {
                        let message = data.toString();
                        let [header, ...content] = message.split(' ');
                        content = JSON.parse(content.join(' '));
                        switch (header) {
                            case 'TERMINAL_ATTACH_RESP':
                                attach_resp_count++;
                                if (attach_resp_count === 1) {
                                    if (content.session !== session1UUID
                                        || content.terminal !== "tablet1"
                                        || content.function !== "numpad"
                                        || content.state.numpad1.solved !== false) {
                                        throw new Error("Invalid response");
                                    }
                                    //step 2: detach
                                    wsAdmin1Session1.send("TERMINAL_ATTACH " + JSON.stringify({
                                        session: session1UUID,
                                        terminal: null
                                    }))
                                } else { // second response (on detach)
                                    if (content.session === session1UUID
                                        && content.terminal === null
                                        && content.function === null
                                        && content.state.numpad1.solved === false) {
                                        resolve();
                                    } else {
                                        reject("Wrong terminal attach response");
                                    }
                                }
                                break;
                            default:
                                reject("Wrong message received: " + header)
                                break;
                        }
                    }
                );
                //step 1: attach
                wsAdmin1Session1.send("TERMINAL_ATTACH " + JSON.stringify({
                    session: session1UUID,
                    terminal: "tablet1"
                }))

                setTimeout(() => {
                    reject("Timeout");
                }, 1000)
            }
        );
    });

    it('Scenario event - as admin - should succeed', async function () {
        let prom1 = new Promise((resolve, reject) => {
                wsAdmin1Session1.on('message', (data) => {
                        let message = data.toString();
                        let [header, ...content] = message.split(' ');
                        content = JSON.parse(content.join(' '));
                        switch (header) {
                            //step 2
                            case 'SCENARIO_EVENT_RESP':
                                if (content.session === session1UUID
                                    && content.event === "some specific description"
                                    && content.state.numpad1.solved === true
                                    && content.state.other.solved === false
                                    && content.state.numpad9.other === 1
                                ) {
                                    resolve();
                                } else {
                                    reject("Wrong scenario event response");
                                }
                                break;
                            default:
                                reject("Wrong message received: " + header)
                                break;
                        }
                    }
                );
                //step 1: scenario event
                wsAdmin1Session1.send("SCENARIO_EVENT " + JSON.stringify({
                    session: session1UUID,
                    event: "some specific description",
                    update: {
                        numpad1: {solved: true},  //on purpose set to true, so value should be overwritten
                        other: {solved: false}  //added something to test merge functionality
                    }
                }))

                setTimeout(() => {
                    reject("Timeout1");
                }, 1000)
            }
        );

        let prom2 = new Promise((resolve, reject) => {
            wsSession1Connection1.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'SCENARIO_UPDATE':
                        if (content.session === session1UUID
                            && content.event === "some specific description"
                            && content.state.numpad1.solved === true
                            && content.state.other.solved === false
                            && content.state.numpad9.other === 1) {
                            resolve();
                        } else {
                            reject("Wrong scenario update response");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout2");
            }, 1000)
        });

        let prom3 = new Promise((resolve, reject) => {
            wsSession1Connection2.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'SCENARIO_UPDATE':
                        if (content.session === session1UUID
                            && content.event === "some specific description"
                            && content.state.numpad1.solved === true
                            && content.state.other.solved === false
                            && content.state.numpad9.other === 1) {
                            resolve();
                        } else {
                            reject("Wrong scenario update response");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout3");
            }, 1000)
        });

        return Promise.all([prom1, prom2, prom3]);
    });

    it('Scenario event - as session - should succeed', async function () {
        let prom1 = new Promise((resolve, reject) => {
                wsSession1Connection1.on('message', (data) => {
                        let message = data.toString();
                        let [header, ...content] = message.split(' ');
                        content = JSON.parse(content.join(' '));
                        switch (header) {
                            //step 2
                            case 'SCENARIO_EVENT_RESP':
                                if (content.session === session1UUID
                                    && content.event === "some specific description"
                                    && content.state.numpad1.solved === true
                                    && content.state.other.solved === false
                                    && content.state.numpad9.other === 1) {
                                    resolve();
                                } else {
                                    reject("Wrong scenario event response");
                                }
                                break;
                            default:
                                reject("Wrong message received: " + header)
                                break;
                        }
                    }
                );
                //step 1: scenario event
                wsSession1Connection1.send("SCENARIO_EVENT " + JSON.stringify({
                    session: session1UUID,
                    event: "some specific description",
                    update: {
                        numpad1: {solved: true},  //on purpose set to true, so value should be overwritten
                        other: {solved: false}  //added something to test merge functionality
                    }
                }))

                setTimeout(() => {
                    reject("Timeout1");
                }, 1000)
            }
        );

        let prom2 = new Promise((resolve, reject) => {
            wsSession1Connection2.on('message', (data) => {
                let message = data.toString();
                let [header, ...content] = message.split(' ');
                content = JSON.parse(content.join(' '));
                switch (header) {
                    case 'SCENARIO_UPDATE':
                        if (content.session === session1UUID
                            && content.event === "some specific description"
                            && content.state.numpad1.solved === true
                            && content.state.other.solved === false
                            && content.state.numpad9.other === 1) {
                            resolve();
                        } else {
                            reject("Wrong scenario update response");
                        }
                        break;
                    default:
                        reject("Wrong message received: " + header)
                        break;
                }
            });

            setTimeout(() => {
                reject("Timeout3");
            }, 1000)
        });

        return Promise.all([prom1, prom2]);
    });


})

// Doc: https://github.com/tdegrunt/jsonschema
// Examples: https://github.com/tdegrunt/jsonschema/blob/HEAD/examples/all.js

/*
    TODO
	session - scenario event without login for that session

    session terminal attach - test detach TERMINAL_EVENT (not implemented yet)!
    session terminal attach - when connection is closed, connection uuid should be removed from terminal (how to test)

	// skip these for now
	admin - scenario events
	admin - reset session
	admin - reset session wrong session uuid
	admin - stop session
	admin - stop session wrong session uuid
	// iets met dot indexing??

 */