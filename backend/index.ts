import {Express} from "express";

let express = require('express');
import dotenv from 'dotenv';
import {Server} from './server';
import {WebSocketExpress, Router} from 'websocket-express';
import WebSocket from 'ws';
import WebSocketExtension from 'ws';

dotenv.config();

const appBase: Express = express();
const port = process.env.PORT;

const app = new WebSocketExpress();
const router = new Router();

let state = new Server();

app.ws('/ws', async (req, res) => {
    const ws: WebSocket & WebSocketExtension = await res.accept();
    state.addClient(ws);
});
//app.use(express.static('static'));

app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});